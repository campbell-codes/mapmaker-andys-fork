from rest_framework import permissions


class CreateOrAdminOnly(permissions.BasePermission):
    """ Custom permission to allow public object create, admin only edit """

    def has_object_permission(self, request, view, obj):
        if request.method == 'POST':
            return True
        if request.user and request.user.is_staff:
            return True
        return False

    def has_permission(self, request, view):
        if request.method == 'POST':
            return True

        if request.user and request.user.is_staff:
            return True
        return False
